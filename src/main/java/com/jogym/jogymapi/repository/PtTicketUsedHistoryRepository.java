package com.jogym.jogymapi.repository;

import com.jogym.jogymapi.entity.PtTicketUsedHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PtTicketUsedHistoryRepository extends JpaRepository<PtTicketUsedHistory, Long> {
}
