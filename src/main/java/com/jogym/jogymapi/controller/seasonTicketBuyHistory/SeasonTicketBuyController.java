package com.jogym.jogymapi.controller.seasonTicketBuyHistory;

import com.jogym.jogymapi.entity.Member;
import com.jogym.jogymapi.entity.SeasonTicket;
import com.jogym.jogymapi.entity.SeasonTicketBuyHistory;
import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.enums.BuyStatus;
import com.jogym.jogymapi.model.common.CommonResult;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.model.seasonticketbuyhistory.SeasonTicketBuyHistoryItem;
import com.jogym.jogymapi.model.seasonticketbuyhistory.SeasonTicketBuyHistoryRequest;
import com.jogym.jogymapi.service.common.ResponseService;
import com.jogym.jogymapi.service.member.MemberService;
import com.jogym.jogymapi.service.seasonticket.SeasonTicketService;
import com.jogym.jogymapi.service.seasonticketbuyhistory.SeasonTicketBuyHistoryService;
import com.jogym.jogymapi.service.storemember.ProfileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "정기권 구매 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/season-ticket-buy")
public class SeasonTicketBuyController {
    private final SeasonTicketBuyHistoryService seasonTicketBuyHistoryService;
    private final MemberService memberService;
    private final SeasonTicketService seasonTicketService;
    private final ProfileService profileService;

    @ApiOperation(value = "정기권 구매")
    @PostMapping("/purchase")
    public CommonResult setSeasonTicketBuyHistory(@RequestBody @Valid SeasonTicketBuyHistoryRequest request){
        StoreMember storeMember = profileService.getMemberData();
        Member member = memberService.getData(request.getMemberId());
        SeasonTicket seasonTicket = seasonTicketService.getOriginSeasonTicket(request.getSeasonTicketId());
        seasonTicketBuyHistoryService.setSeasonTicketBuyHistory(member, storeMember, seasonTicket);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "최근 방문일 수정")
    @PutMapping("/date-last/buy-history-id/{buyHistoryId}")
    public CommonResult putHistoryDateLast(@PathVariable long buyHistoryId){
        StoreMember storeMember = profileService.getMemberData();
        seasonTicketBuyHistoryService.putHistoryDateLast(storeMember, buyHistoryId);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "구매내역 상태 변경")
    @PutMapping("/buy-status/buy-history-id/{buyHistoryId}")
    public CommonResult putHistoryBuyStatus(@PathVariable long buyHistoryId, BuyStatus buyStatus){
        StoreMember storeMember = profileService.getMemberData();
        seasonTicketBuyHistoryService.putSeasonTicketBuyHistoryBuyStatus(storeMember, buyHistoryId, buyStatus);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "구매내역 리스트")
    @GetMapping("/list/member-id/{memberId}")
    public ListResult<SeasonTicketBuyHistoryItem> getSeasonTicketBuyHistory(@PathVariable long memberId, @RequestParam(value = "page",required = false, defaultValue = "1") int page){
        StoreMember storeMember = profileService.getMemberData();

        return ResponseService.getListResult(seasonTicketBuyHistoryService.getSeasonTicketBuyHistory(memberId, storeMember,page), true);
    }
}