package com.jogym.jogymapi.exception;

public class CAlreadyUsingSeasonTicketException extends RuntimeException {
    public CAlreadyUsingSeasonTicketException(String msg, Throwable t) {
        super(msg, t);
    }

    public CAlreadyUsingSeasonTicketException(String msg) {
        super(msg);
    }

    public CAlreadyUsingSeasonTicketException() {
        super();
    }
}
