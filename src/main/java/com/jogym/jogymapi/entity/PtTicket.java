package com.jogym.jogymapi.entity;

import com.jogym.jogymapi.interfaces.CommonModelBuilder;
import com.jogym.jogymapi.model.ptticket.PtTicketRequest;
import com.jogym.jogymapi.model.ptticket.PtTicketUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PtTicket {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 회원등록일
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    // 가맹점id
    @JoinColumn(name = "storeMemberId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private StoreMember storeMember;

    // 직원id
    @JoinColumn(name = "trainerMemberId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private TrainerMember trainerMember;

    // PT권명
    @Column(nullable = false, length = 20)
    private String ticketName;

    // 최대횟수
    @Column(nullable = false)
    private Integer maxCount;

    // 요금
    @Column(nullable = false)
    private Double unitPrice;

    // 사용유무
    @Column(nullable = false)
    private Boolean isEnabled;

    public void putPtTicket(PtTicketUpdateRequest request) {
        this.maxCount = request.getMaxCount();
        this.unitPrice = request.getUnitPrice();
    }

    public void delPtTicket() {
        isEnabled = false;
    }

    private PtTicket(PtTicketBuilder builder) {
        this.dateCreate = builder.dateCreate;
        this.storeMember = builder.storeMember;
        this.trainerMember = builder.trainerMember;
        this.ticketName = builder.ticketName;
        this.maxCount = builder.maxCount;
        this.unitPrice = builder.unitPrice;
        this.isEnabled = builder.isEnabled;
    }

    public static class PtTicketBuilder implements CommonModelBuilder<PtTicket> {
        private final LocalDateTime dateCreate;
        private final StoreMember storeMember;
        private final TrainerMember trainerMember;
        private final String ticketName;
        private final Integer maxCount;
        private final Double unitPrice;
        private final Boolean isEnabled;

        public PtTicketBuilder(StoreMember storeMember, TrainerMember trainerMember, PtTicketRequest request) {
            this.dateCreate = LocalDateTime.now();
            this.storeMember = storeMember;
            this.trainerMember = trainerMember;
            this.ticketName = request.getTicketName();
            this.maxCount = request.getMaxMonth();
            this.unitPrice = request.getUnitPrice();
            this.isEnabled = true;
        }

        @Override
        public PtTicket build() {
            return new PtTicket(this);
        }
    }
}
