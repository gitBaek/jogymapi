package com.jogym.jogymapi.entity;

import com.jogym.jogymapi.enums.Gender;
import com.jogym.jogymapi.interfaces.CommonModelBuilder;
import com.jogym.jogymapi.model.trainermember.TrainerMemberRequest;
import com.jogym.jogymapi.model.trainermember.TrainerMemberUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TrainerMember {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 회원등록일
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    // 가맹점id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "storeMemberId", nullable = false)
    private StoreMember storeMember;

    // 이름
    @Column(nullable = false, length = 20)
    private String name;

    // 연락처
    @Column(nullable = false, length = 13)
    private String phoneNumber;

    // 주소
    @Column(nullable = false, length = 100)
    private String address;

    // 성별
    @Column(nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private Gender gender;

    // 생년월일
    @Column(nullable = false)
    private LocalDate dateBirth;

    // 경력 및 자격사항
    @Column(columnDefinition = "TEXT")
    private String careerContent;

    // 비고
    @Column(columnDefinition = "TEXT")
    private String memo;

    // 사용유무
    @Column(nullable = false)
    private Boolean isEnabled;

    public void putTrainerMember(TrainerMemberUpdateRequest request) {
        this.name = request.getName();
        this.phoneNumber = request.getPhoneNumber();
        this.address = request.getAddress();
        this.gender = request.getGender();
        this.dateBirth = request.getDateBirth();
        this.careerContent = request.getCareerContent();
        this.memo = request.getMemo();
    }

    // 트레이너 활성화 변경 --> true에서 false로 변경
    // 트레이너 삭제
    public void putTrainerMemberDelete() {
        this.isEnabled = false;
    }


    private TrainerMember(TrainerMemberBuilder builder) {
        this.dateCreate = builder.dateCreate;
        this.storeMember = builder.storeMember;
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
        this.address = builder.address;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.isEnabled = builder.isEnabled;
        this.careerContent = builder.careerContent;
        this.memo = builder.memo;
    }

    public static class TrainerMemberBuilder implements CommonModelBuilder<TrainerMember> {
        private final LocalDateTime dateCreate;
        private final StoreMember storeMember;
        private final String name;
        private final String phoneNumber;
        private final String address;
        private final Gender gender;
        private final LocalDate dateBirth;
        private final Boolean isEnabled;
        private final String careerContent;
        private final String memo;

        public TrainerMemberBuilder(StoreMember storeMember, TrainerMemberRequest request) {
            this.dateCreate = LocalDateTime.now();
            this.storeMember = storeMember;
            this.name = request.getName();
            this.phoneNumber = request.getPhoneNumber();
            this.address = request.getAddress();
            this.gender = request.getGender();
            this.dateBirth = request.getDateBirth();
            this.isEnabled = true;
            this.careerContent = request.getCareerContent();
            this.memo = request.getMemo();
        }
        @Override
        public TrainerMember build() {
            return new TrainerMember(this);
        }
    }
}
