package com.jogym.jogymapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MemberGroup {
    ROLE_ADMIN("최고관리자")
    , ROLE_STORE("가맹점")
    ;

    private final String name;
}
